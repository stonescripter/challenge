# Atlassian Challenge

## Overview
I wrote the core program in Java, utilizing Spring framework and Gradle. (https://start.spring.io/).

This utilizes a couchDb backend for the storing the JSON Account and Contact objects. 


*note*:  I REALLY wanted to containerize the entire thing, all wrapped up nicely in a Docker-Compose file, however I was not able to get this done within a reasonable amount of time. 
This would be the next step were I to continue working on this. So - the componets are separate, but I'll go over how to spin everything up and use it. 

**_update_**

**I created a new branch (v2) in which I packaged this up much better. In v2, both the db and app are containerized and can be launched via a single shell script, run.sh**

**I created a new branch instead of adding to this once since its outside the scope of the initial timed assignemnt - But i couldnt stand leaving it and needed to package it for sanity**

**_update_**

*Known Issue*:  **(CHAL-1)** Contact objects are stored both as a Contact and within an Account object. This would need to be fixed. 
For example, if I update the Contact 'Tanjiro' as part of a new Account creation, the 'Tanjiro' Contact object will also be updated BUT any earlier Account objects 'Tanjiro' was a part of will still contain his old information.

## Environment
First, spin up the backend db. This just pulls the couchdb image from docker hub, and sets admin/password as well as exposes the port on localhost

```docker run -p 5984:5984 -d --name my-couchdb -e COUCHDB_USER=admin -e COUCHDB_PASSWORD=password couchdb:latest```

Next, you can spin up the service with ```./gradlew bootRun``` from within the /challenge (top) directory. This should start up the service assuming java and everything plays nicely. I sincerely apologize again that this is not neatly packed in a container. 


If you want to view the Couchdb web interface: http://127.0.0.1:5984/_utils/#login (admin/password)
## Testing
I included a PostMan collection at the top level of the project. This will be the best way to test the APIs. They do not return nice response codes.

## Examples

### Account POST (http://localhost:8080/account/create)

Creates a new Account as well as any nested Contacts. 

Returns 500 if Account already exists. 

```json
	{
		"_id": "Ufotable",
		"addressLine1": "400 S St",
		"addressLine2": "",
		"city": "Tokyo",
		"state": "",
		"zip": "90210",
		"country": "Japan",
		"contacts": [{
			"_id": "Nezuko",
			"email": "default_email@default.com",
			"addressLine1": "400 S St",
			"addressLine2": "",
			"city": "Austin",
			"state": "TX",
			"zip": "90210",
			"country": "USA"
		}]
	}
```
### Account PUT (http://localhost:8080/account/update)

Updates existing account as well as any nested Contacts. 

Returns 500 if Account does not exist.

Will create new Contacts if they do not exist and have been added to the Account.

```json
{
    "_id": "Ufotable",
    "addressLine1": "400 S St",
    "addressLine2": "",
    "city": "Tokyo",
    "state": "",
    "zip": "90210",
    "country": "Japan",
    "contacts": [
        {
            "_id": "Nezuko",
            "email": "default_email@default.com",
            "addressLine1": "400 S St",
            "addressLine2": "",
            "city": "Austin",
            "state": "TX",
            "zip": "90210",
            "country": "USA"
        },
        {
            "_id": "Tanjiro",
            "email": "default_email@default.com",
            "addressLine1": "400 S St",
            "addressLine2": "",
            "city": "Austin",
            "state": "TX",
            "zip": "90210",
            "country": "USA"
        }
    ]
}
```

### Account GET (http://localhost:8080/account/{name})

Returns Account

### Account GET Contacts (http://localhost:8080/account/{name}/contacts)

Returns Contacts of a specific Account

### Contact POST (http://localhost:8080/contact/create)

Creates new Contact

Returns 500 if Contact already exists.

```json
{
    "_id": "Inosuke",
    "email": "default_email@default.com",
    "addressLine1": "400 S St",
    "addressLine2": "",
    "city": "Nagasaki",
    "state": "JP",
    "zip": "90210",
    "country": "JAPAN"
}
```

### Contact PUT (http://localhost:8080/contact/update)

Updates existing Contact

Returns 500 if Contact does not exist.

```json
{
    "_id": "Inosuke",
    "email": "updated_email@default.com",
    "addressLine1": "400 S St",
    "addressLine2": "",
    "city": "Tokyo",
    "state": "JP",
    "zip": "90210",
    "country": "JAPAN"
}
```

### Contact GET (http://localhost:8080/contact/{name})

Returns a Contact