package com.example.challenge;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
//couchdb support
import java.net.MalformedURLException;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;
import org.ektorp.support.DesignDocument;
import org.ektorp.ViewQuery;

@RestController
@RequestMapping("contact")
public class ContactController {

	private CouchDbConnector db;

	public ContactController() {
		try {
			HttpClient httpClient = new StdHttpClient.Builder().url("http://localhost:5984")
				.username("admin")
				.password("password")
				.build();
			CouchDbInstance dbInstance = new StdCouchDbInstance(httpClient);
			this.db = new StdCouchDbConnector("test", dbInstance);
			db.createDatabaseIfNotExists();
		} catch(Exception e) {
			System.out.println("Failed to connect to db. " + e.toString());
		}
		
	}
	
	@GetMapping("/{name}")
	public Contact getContact(@PathVariable String name) {
		return db.get(Contact.class, name);
	}

	@PutMapping("/update")
	public void updateContact(@RequestBody Contact contact) {
		Contact existingContact = db.get(Contact.class, contact.getName());
		contact.setRevision(existingContact.getRevision());
 		db.update(contact);
	}

	@PostMapping("/create")
	public void createContact(@RequestBody Contact contact) {
 		db.create(contact);
	}
}