package com.example.challenge;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
//couchdb support
import java.net.MalformedURLException;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;
import org.ektorp.support.DesignDocument;
import org.ektorp.ViewQuery;

@RestController
@RequestMapping("account")
public class AccountController {

	private CouchDbConnector db;

	public AccountController() {
		try {
			HttpClient httpClient = new StdHttpClient.Builder().url("http://localhost:5984")
				.username("admin")
				.password("password")
				.build();
			CouchDbInstance dbInstance = new StdCouchDbInstance(httpClient);
			this.db = new StdCouchDbConnector("test", dbInstance);
			db.createDatabaseIfNotExists();
		} catch(Exception e) {
			System.out.println("Failed to connect to db. " + e.toString());
		}
		
	}
	
	@GetMapping("/{name}")
	public Account getAccount(@PathVariable String name) {
		return db.get(Account.class, name);
	}

	@GetMapping("/{name}/contacts")
	public Contact[] getAccountContacts(@PathVariable String name) {
		Account accnt = db.get(Account.class, name);
		return accnt.getContacts();
	}

	@PutMapping("/update")
	public void updateAccount(@RequestBody Account account) {
		Account existingAccount = db.get(Account.class, account.getName());
		account.setRevision(existingAccount.getRevision());
 		db.update(account);
		for (Contact c : account.getContacts()) {
			try {
				Contact existingContact = db.get(Contact.class, c.getName());
				
				c.setRevision(existingContact.getRevision());
				db.update(c);
			} catch (Exception e) {
				System.out.println("Contact not found. Creating new");
				db.create(c);
			}
			
		}
	}

	@PostMapping("/create")
	public void createAccount(@RequestBody Account account) {
		System.out.println("HERE");
 		db.create(account);

		// create individual contacts..
		 for (Contact c : account.getContacts()) {
			try {
			 	db.create(c);
			} catch (Exception e) {
				System.out.println("Contact already exists - try updating");
				try {
					Contact existingContact = db.get(Contact.class, c.getName());
					
					c.setRevision(existingContact.getRevision());
					db.update(c);
				} catch (Exception e2) {
					System.out.println("Something Unexpected Happened here");
				}
			}
		 }
	}
}