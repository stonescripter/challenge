package com.example.challenge;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.ektorp.support.CouchDbRepositorySupport;


@JsonIgnoreProperties({"id", "revision"})
public class Account {

    @JsonProperty("_id")
    private String name;

    @JsonProperty("_rev")
    private String revision;

    private String addressLine1;
    private String addressLine2;
    private String city;
    private String state;
    private String zip;
    private String country;
    private Contact[] contacts;

    public String getRevision() {
        return revision;
    }

    public void setRevision(String s) {
        revision = s;
    }

    public void setName(String s) {
        name = s;
    }

    public String getName() {
        return name;
    }

    public void setAddressLine1(String s) {
        addressLine1 = s;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine2(String s) {
        addressLine2 = s;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setCity(String s) {
        city = s;
    }

    public String getCity() {
        return city;
    }

    public void setState(String s) {
        state = s;
    }

    public String getState() {
        return state;
    }

    public void setZip(String s) {
        zip = s;
    }

    public String getZip() {
        return zip;
    }

    public void setCountry(String s) {
        country = s;
    }

    public String getCountry() {
        return country;
    }

    public void setContacts(Contact[] c) {
        contacts = c;
    }

    public Contact[] getContacts() {
        return contacts;
    }
}